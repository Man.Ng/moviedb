import React, {Component} from 'react';

import GetData from './data/getData';

import Header from './components/header/header';
import Filter from './components/filter/filter';
import MainContent from './components/mainContent/mainContent'; 
import Modal from './components/modal/modal';

class App extends Component {

  STORE = {
    movie: [],
    tv: [],
    person: []
  };

  constructor(props) {
    super(props)
    this.state = {
      currentList: [],
      isActive: false,
      articleContent: []
    }
  }

  componentDidMount() {
    this.updateMovieData("movie");
  }

  async updateMovieData(key) {
    if (!this.STORE[key] || !this.STORE[key].length) {
      const list = await GetData(key);
      this.STORE[key] = list;
    }
    
    this.setState({ currentList: [...this.STORE[key]] });
  }

  setResults(newResults) {
    switch(newResults) {
      case 'movies':
        this.updateMovieData("movie")
        break;
      case 'tv':
        this.updateMovieData("tv")
        break;
      case 'people':
        this.updateMovieData("person")
        break;
    }
  }

  toggleModal() {
    this.setState(prevState => ({
      isActive: !prevState.isActive
    }));
  }

  setContent(content) {
    this.setState({ articleContent: content });
  }

  render () {
    return (
      <div className="app">
        <Header/>
        <Filter useFilter={(selected) => this.setResults(selected)}/>
        <MainContent 
          listItems={this.state.currentList} 
          getContent={(content) => this.setContent(content)} 
          openModal={() => this.toggleModal()}
        />
        <Modal 
          modalIsActive={this.state.isActive} 
          articleContent={this.state.articleContent} 
          closeModal={() => this.toggleModal()}
        />
      </div>
    )
  }
}

export default App;
