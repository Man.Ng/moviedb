import React, {Component} from 'react';

import './header.scss';

class Header extends Component {
  render() {
    return (
      <header>
        <h1>Popular Movies, Tv shows and Actors/Actresses</h1>
        <span>THE MOVIE Database API</span>
      </header>
    )
  }
}

export default Header;