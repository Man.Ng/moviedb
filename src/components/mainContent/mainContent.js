import React, {Component} from 'react';
import './mainContent.scss';

class mainContent extends Component {
  
  state = {
    activeModal: false
  }

  render() {
    let listItems = this.props.listItems;

    return (
      <div className="mainContent">
        <ul>
          {listItems.map((results, index) => {
            return (
              <li key={index} onClick={() => { this.props.openModal(); this.props.getContent(results)}}>
                <img className="result-image" alt={results.title} title={results.title} src={`https://image.tmdb.org/t/p/w500/${results.poster_path ? results.poster_path : results.profile_path }`}/>
                <span className="result-title">{results.title ? results.title : results.name }</span>
              </li>
            )
          })}
        </ul>
      </div>
    )
  }
}

export default mainContent;