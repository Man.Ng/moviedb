import React,{Component} from 'react';

import GeneralTemplate from './generalTemplate';
import PeopleTemplate from './peopleTemplate';

import './modal.scss';

class Modal extends Component {
  render() {
    let active = this.props.modalIsActive,
        content = this.props.articleContent;

    return (
      <div className={`modal ${active ? "active" : "not-active"}`}>
        <div className="article-details-container">
          <div className="article-details">
            <span onClick={() => this.props.closeModal()} className="article-details__close">Close</span>

            { content.poster_path ? 
              <GeneralTemplate articleContent={content} /> : <PeopleTemplate articleContent={content} />
            }
          </div>
        </div> 
      </div>
    )
  }
}

export default Modal;