import React, {Component} from 'react';

class GeneralTemplate extends Component {
  
  render() {
    let content = this.props.articleContent,
        knownFor = content.known_for;

        if (!knownFor || !knownFor.length) {
          return null;
        }
        
    return (
      <React.Fragment>
        <img className="article-details__image" alt={content.title} title={content.title} src={`https://image.tmdb.org/t/p/w500/${content.profile_path}`}/>
        <div className="article-details__info">
          <h2>{content.name}</h2>
          <h4>Known for:</h4>
          <ul className="article-details__known-for">
            {knownFor.map((results, index) => {
              return (
                <li key={index}>
                  <img src={`https://image.tmdb.org/t/p/w500/${results.poster_path}`}/>
                  <span>{results.original_title}</span>
                </li>
              )
            })}
          </ul>
        </div> 
      </React.Fragment>
    )
  }
}

export default GeneralTemplate;