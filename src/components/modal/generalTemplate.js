import React, {Component} from 'react';

class GeneralTemplate extends Component {
  render() {
    let content = this.props.articleContent;

    return (
      <React.Fragment>
        <img className="article-details__image" alt={content.title} title={content.title} src={`https://image.tmdb.org/t/p/w500/${content.poster_path}`}/>
        <div className="article-details__info">
          <h2>{content.original_title ? content.original_title : content.name}</h2>
          <h4>Overview</h4>
          <p>{content.overview}</p>
        </div> 
      </React.Fragment>
    )
  }
}

export default GeneralTemplate;