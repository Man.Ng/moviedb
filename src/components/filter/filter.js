import React, {Component} from 'react';

import './filter.scss';

class Filter extends Component {
  render() {
    return (
      <div className="filter">
        <button onClick={() => {this.props.useFilter("movies")}}>Movies</button>
        <button onClick={() => {this.props.useFilter("tv")}}>Tv</button>
        <button onClick={() => {this.props.useFilter("people")}}>People</button>
      </div>
    )
  }
}

export default Filter;