function GetData(type) {
  return fetch("https://api.themoviedb.org/3/"+ type + "/popular?api_key=d0aea524bd07ed49cbc26dff63f357dd")
  .then(res => res.json())
  .then(res => {
    return res.results;
  })
  .catch(err => {
    console.log(err);
    return [];
  })
}

export default GetData;